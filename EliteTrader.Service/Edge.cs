﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EliteTrader.Service
{
    public class Edge<TState>
    {
        public Edge<TState> Parent { get; set; }
        public TState State { get; set; }
        public float Fitness { get; set; }

        public override string ToString()
        {
            return string.Format("f:{0:#,##0.0} s:{1}", Fitness, State);
        }
    }

    public static class EdgeExtensions
    {

        public static IEnumerable<Edge<TState>> EnumerateUpParents<TState>(this Edge<TState> me)
        {
            var current = me;
            while (current != null)
            {
                yield return current;
                current = current.Parent;
            }
        }

    }

}
