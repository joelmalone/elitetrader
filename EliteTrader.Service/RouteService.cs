﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EliteTrader.Service
{

    public class RouteService
    {

        private class EdgeFitnessComparer<TState> : IComparer<Edge<TState>>
        {
            int IComparer<Edge<TState>>.Compare(Edge<TState> x, Edge<TState> y)
            {
                return (int)Math.Round(x.Fitness - y.Fitness, MidpointRounding.AwayFromZero);
            }
        }

        public IEnumerable<Edge<TState>> Solve<TState>(TState initialState, Func<TState, bool> successFunc, Func<Edge<TState>, float> fitnessFunc, Func<TState, IEnumerable<TState>> nextStatesFunc)
        {
            var fitnessComparer = new EdgeFitnessComparer<TState>();
            var untestedEdges = new List<Edge<TState>>();
            var testedEdges = new Dictionary<TState, Edge<TState>>();

            var initialStateEdge = new Edge<TState>
            {
                Fitness = 0,
                Parent = null,
                State = initialState,
            };
            untestedEdges.Add(initialStateEdge);

            while (untestedEdges.Count > 0)
            {
                // Recompute the fitness for all untested edges so we can operate
                //  on the best ones first
                foreach (var i in untestedEdges)
                    i.Fitness = fitnessFunc(i);
                untestedEdges.Sort(fitnessComparer);
                // Grab the best edge and remove it from the list; we sort on ascending
                //  fitness so the best is always the last, making for a fast removal.
                var lastIndex = untestedEdges.Count - 1;
                var edge = untestedEdges[lastIndex];
                untestedEdges.RemoveAt(lastIndex);

                if (successFunc(edge.State))
                    yield return edge;

                // Check if this state already has a path to it, i.e. have we
                //  arrived at this state already in our testing
                Edge<TState> existingStateEdge;
                if (testedEdges.TryGetValue(edge.State, out existingStateEdge))
                {
                    var existingStateFitness = fitnessFunc(existingStateEdge);
                    if (edge.Fitness > existingStateFitness)
                    {
                        // We have found a better path to this state, so replace it
                        existingStateEdge.Parent = edge;
                        existingStateEdge.Fitness = edge.Fitness;
                        // Note that we don't need to add any untested edges, as that
                        //  would have already been queued up when the replaced edge
                        //  was tested
                    }
                    else
                    {
                        // A better path exists to this state, so discard this one
                    }
                }
                else
                {
                    // This state does not exist, so add it and queue it for testing
                    testedEdges[edge.State] = edge;
                    // Queue up the next edges from this edge
                    foreach (var nextState in nextStatesFunc(edge.State))
                    {
                        untestedEdges.Add(new Edge<TState>
                        {
                            // No need to set fitness
                            Parent = edge,
                            State = nextState,
                        });
                    }
                }
            }
        }


    }

}
