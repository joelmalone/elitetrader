﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EliteTrader.Web.Startup))]
namespace EliteTrader.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
