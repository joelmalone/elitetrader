﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace EliteTrader.Service.Tests
{
    [TestClass]
    public class RouteServiceTests
    {

        /// <summary>
        /// This test finds a word in an unlimited-size network of 
        /// upper- and lower-case letters.  Fitness is increased by the 
        /// number of correct letters (in-place) and decreased by the
        /// length off the potential solution.  Not a great test of an A*
        /// search, but shows the importance of correctly weighting the 
        /// fitness for good results to guide the search towards the 
        /// solution rather than arbitrarily traversing the search space.
        /// </summary>
        [TestMethod]
        public void SimpleTextTest()
        {
            var nodes = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
                .Select(char.ToString);
            var service = new RouteService();

            const string expectedSolution = "Piglet";

            // This test is pretty dumb because there's only 1 possible path,
            //  but it does have a hugely connected graph, so is a good test
            //  of the sorting to find the next best edge to test
            var solution = service.Solve("",
                (state) => state.Equals(expectedSolution),
                (edge) =>
                {
                    var correctInPlace = Enumerable.Range(0, Math.Min(expectedSolution.Length, edge.State.Length))
                        .Where(i => expectedSolution[i].Equals(edge.State[i]))
                        .Count();
                    return correctInPlace * correctInPlace - edge.State.Length;
                },
                (state) => nodes.Select(n => state + n)
                ).First();

            Assert.AreEqual(expectedSolution, solution.State);
        }

        /// <summary>
        /// Find the shortest route from the start node, through 
        /// some waypoints, then back to the start node.
        /// 
        /// This tests a complicated TState equality test in that 
        /// we are solving more than just a path to the target node.
        /// </summary>
        [TestMethod]
        public void ClosedLoopViaWaypointsTest()
        {
            /* Starting at a, visit nodes c and f then return to a.
             *     b--e
             *    /    \
             *   a   c  f
             *    \ /  /
             *     d  g
             */
            var connectivity = new Dictionary<char, string>
            {
                { 'a', "bd" },
                { 'b', "ae" },
                { 'c', "d" },
                { 'd', "ac" },
                { 'e', "bf" },
                { 'f', "eg" },
                { 'g', "f" },
            };

            var service = new RouteService();
            var solutions = service.Solve(
                new ClosedLoopViaWaypointsState
                {
                    Node = 'a',
                },
                (state) => state.Node == 'a'
                    && state.WaypointCVisited
                    && state.WaypointFVisited,
                (edge) => -edge.EnumerateUpParents().Count(),
                (state) => connectivity[state.Node].Select(n => new ClosedLoopViaWaypointsState
                {
                    Node = n,
                    WaypointCVisited = state.WaypointCVisited || n == 'c',
                    WaypointFVisited = state.WaypointFVisited || n == 'f',
                })
            );
            var solution = solutions.Last();

            var solutionPath = solution
                .EnumerateUpParents()
                .Reverse()
                .Select(e => e.State.Node)
                .ToList();
            CollectionAssert.AreEqual("adcdabefeba".ToList(), solutionPath);
        }

        private struct ClosedLoopViaWaypointsState
        {
            public char Node { get; set; }
            public bool WaypointCVisited { get; set; }
            public bool WaypointFVisited { get; set; }

            public override bool Equals(object obj)
            {
                var other = (ClosedLoopViaWaypointsState)obj;
                return other.Node.Equals(this.Node)
                    && other.WaypointCVisited == this.WaypointCVisited
                    && other.WaypointFVisited == this.WaypointFVisited;
            }

            public override int GetHashCode()
            {
                return Node.GetHashCode()
                    ^ (WaypointCVisited ? 0x43e1 : 0x9a37)
                    ^ (WaypointFVisited ? 0xfceb : 0x0f3d);
            }

            public override string ToString()
            {
                return string.Format("'{0}' C: {1} F: {2}", Node, WaypointCVisited, WaypointFVisited);
            }
        }

    }
}
